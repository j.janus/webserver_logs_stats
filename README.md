# SpTest

## Howto

First install all required dependencies

    $ bundle install

### Running tests

    $ bundle exec rspec

### CLI

There is a script which serves CLI for the program in `bin/web_stats.rb`

You can get all available commands by running:

    $ bin/web_stats.rb

#### Generate page views report
    
    $ bin/web_stats.rb pageViews path/to/logfile.log

#### Generate unique page visits report

    $ bin/web_stats.rb uniqViews path/to/logfile.log

#### Generate all reports

    $ bin/web_stats.rb allStats path/to/logfile.log

## Architecture

### Production code design

Production code is organized in 3 major layers (as shown on the diagram)

1. User Interface (CLI) - single file with CLI commands definitions in `bin/web_stats.rb`
2. Application logic / orchestration - public facade of the gem's `SpTest::WebsiteMonitorClient`
3. Domain code - all domain objects

![Code architecture diagram](docs/architecture.png "Architecture")

### Tests

Tests are divided into two main categories:

1. unit tests - in `spec/unit` which test internal components (classes) behavior
2. acceptance tests - in `spec/acceptance` which test the main program's functionality (generating reports) based on some example logfiles (located in `spec/logfiles_examples`) 