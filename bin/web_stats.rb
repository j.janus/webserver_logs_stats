#!/usr/bin/env ruby

require 'bundler/setup'
require 'sp_test'
require 'dry/cli'


module WebStats
  module CLI
    module Commands
      extend Dry::CLI::Registry

      class Version < Dry::CLI::Command
        desc 'Print version'

        def call(*)
          puts '1.0.0'
        end
      end

      class PageViews < Dry::CLI::Command
        desc 'Loads logs from :logfile and generates report for most viewed pages'

        argument :logfile, desc: 'path to logfile containing log entries'

        example [
          'path/to/logfile.log'
        ]

        def call(logfile: nil, **)
          client = SpTest::WebsiteMonitorClient.new
          client.load(file_path: logfile)

          all_views_report = client.generate_report(:most_views)

          puts all_views_report.to_s
        rescue SpTest::Error => e
          puts "[Error] #{e.message}"
        end
      end

      class UniqPageViews < Dry::CLI::Command
        desc 'Loads logs from :logfile and generates report for most uniquely viewed pages'

        argument :logfile, desc: 'path to logfile containing log entries'

        example [
          'path/to/logfile.log'
        ]

        def call(logfile: nil, **)
          client = SpTest::WebsiteMonitorClient.new
          client.load(file_path: logfile)

          uniq_views_report = client.generate_report(:most_uniq_views)

          puts uniq_views_report.to_s
        rescue SpTest::Error => e
          puts "[Error] #{e.message}"
        end
      end

      class AllStats < Dry::CLI::Command
        desc 'Loads logs from :logfile and generates all reports (page views and unique page views)'

        argument :logfile, desc: 'path to logfile containing log entries'

        example [
          'path/to/logfile.log'
        ]

        def call(logfile: nil, **)
          client = SpTest::WebsiteMonitorClient.new
          client.load(file_path: logfile)

          all_views_report = client.generate_report(:most_views)
          uniq_views_report = client.generate_report(:most_uniq_views)

          puts <<~OUTPUT
            #{all_views_report}

            #{uniq_views_report}
          OUTPUT
        rescue SpTest::Error => e
          puts "[Error] #{e.message}"
        end
      end

      register 'version', Version, aliases: %w[v -v --version]
      register 'pageViews', PageViews, aliases: %w[pv views]
      register 'uniqViews', UniqPageViews, aliases: %w[uv uniq]
      register 'allStats', AllStats, aliases: %w[all stats]
    end
  end
end

Dry::CLI.new(WebStats::CLI::Commands).call
