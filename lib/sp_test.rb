# frozen_string_literal: true

require 'sp_test/version'
require 'sp_test/website_monitor'
require 'sp_test/log_reader'
require 'sp_test/report_generators'
require 'sp_test/value_objects/page_view'
require 'sp_test/value_objects/page_stats'

module SpTest
  class Error < StandardError; end

  class WebsiteMonitorClient
    VALID_REPORT_TYPES = %i[most_views most_uniq_views].freeze
    private_constant :VALID_REPORT_TYPES

    def initialize
      @log_reader = LogReader.new
      @website_monitor = WebsiteMonitor.new
      @report_generators = {
        most_views: ReportGenerators::MostViewsReportGenerator.new,
        most_uniq_views: ReportGenerators::MostUniqueViewsReportGenerator.new
      }
    end

    def load(file_path:)
      page_views = @log_reader.read(file_path)
      page_views.each { |page_view| @website_monitor.track_page_view(page_view) }
    rescue LogReader::FileNotFound => e
      raise Error, e.message
    end

    def generate_report(report_type)
      unless VALID_REPORT_TYPES.include?(report_type)
        raise Error, "Unknown report type: #{report_type}, valid types are: #{VALID_REPORT_TYPES}"
      end

      page_stats = @website_monitor.all_visited_pages_stats
      @report_generators[report_type].generate(page_stats)
    end
  end
end
