# frozen_string_literal: true

module SpTest
  class LogParser
    def parse(raw_log)
      match_data = /(.+)\s(.+)/.match(raw_log)
      ValueObjects::PageView.new(page: match_data.captures[0], client_id: match_data.captures[1])
    end
  end

  class LogReader
    class FileNotFound < StandardError; end

    FILTER_EMPTY_LINES = ->(line) { !/^\s+/.match?(line) }
    private_constant :FILTER_EMPTY_LINES

    def initialize
      @parser = LogParser.new
    end

    def read(file_path)
      file = File.open(file_path)
      raw_logs = file.readlines
      filtered_logs = raw_logs.filter &FILTER_EMPTY_LINES
      filtered_logs.map { |raw| @parser.parse(raw) }
    rescue Errno::ENOENT => e
      raise FileNotFound, "Could not find #{file_path} file"
    ensure
      file.close if file
    end
  end
end
