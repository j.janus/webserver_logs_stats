# frozen_string_literal: true

module SpTest
  module ReportGenerators
    Entry = Struct.new(:page, :value, :description) do
      def to_s
        "#{page} #{value} #{description}"
      end
    end

    Report = Struct.new(:entries, :name) do
      def to_s
        rows = entries.map { |entry| "  #{entry.to_s}" }

        <<~REPORT
          #{name}
          #{rows.join("\n")}
        REPORT
      end
    end

    class SimpleReportGenerator
      def initialize(name:, sort_by:, entries_mapper:)
        @name = name
        @sort_by = sort_by
        @entries_mapper = entries_mapper
      end

      def generate(page_stats)
        ordered_stats = page_stats.sort &@sort_by
        entries = ordered_stats.map &@entries_mapper

        Report.new(entries, @name)
      end
    end
    private_constant :SimpleReportGenerator

    class MostViewsReportGenerator
      NAME = 'Most popular pages'
      private_constant :NAME

      def initialize
        @generator = SimpleReportGenerator.new(
          name: NAME,
          sort_by: ->(s1, s2) { s2.views <=> s1.views },
          entries_mapper: ->(stat) { Entry.new(stat.page, stat.views, 'visits') }
        )
      end

      def generate(page_stats)
        @generator.generate(page_stats)
      end
    end

    class MostUniqueViewsReportGenerator
      NAME = 'Pages with most unique views'
      private_constant :NAME

      def initialize
        @generator = SimpleReportGenerator.new(
          name: NAME,
          sort_by: ->(s1, s2) { s2.uniq_views <=> s1.uniq_views },
          entries_mapper: ->(stat) { Entry.new(stat.page, stat.uniq_views, 'unique views') }
        )
      end

      def generate(page_stats)
        @generator.generate(page_stats)
      end
    end
  end
end