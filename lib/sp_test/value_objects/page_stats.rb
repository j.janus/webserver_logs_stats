# frozen_string_literal: true

require 'dry-struct'

module SpTest
  module ValueObjects
    class PageStats < Dry::Struct
      attribute :page, Dry.Types()::String
      attribute :views, Dry.Types()::Integer
      attribute :uniq_views, Dry::Types()::Integer
    end
  end
end
