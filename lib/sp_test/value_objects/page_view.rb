# frozen_string_literal: true

require 'dry-struct'

module SpTest
  module ValueObjects
    class PageView < Dry::Struct
      attribute :page, Dry.Types()::String
      attribute :client_id, Dry.Types()::String
    end
  end
end
