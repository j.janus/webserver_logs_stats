# frozen_string_literal: true

module SpTest
  PageStatsTracker = Struct.new(:page, :views, :uniq_views) do
    def initialize(page:, views: 0, uniq_views: 0)
      @clients_viewed = {}
      super(page, views, uniq_views)
    end

    def record_view(by_client)
      self.views = views + 1
      return if @clients_viewed.key?(by_client)

      @clients_viewed[by_client] = true
      self.uniq_views = uniq_views + 1
    end

    def to_page_stats
      ValueObjects::PageStats.new(page: page, views: views, uniq_views: uniq_views)
    end
  end

  # class responsible for tracking website traffic
  class WebsiteMonitor
    def initialize
      @page_stats_trackers = {}
    end

    def track_page_view(page_view)
      page = page_view.page
      page_stats_tracker = fetch_page_stats_tracker(page) || init_page_stats_tracker(page)

      page_stats_tracker.record_view(page_view.client_id)
    end

    def all_visited_pages_stats
      @page_stats_trackers.values.map(&:to_page_stats)
    end

    def stats_for(page)
      @page_stats_trackers[page]&.to_page_stats
    end

    private

    def fetch_page_stats_tracker(page)
      @page_stats_trackers[page]
    end

    def init_page_stats_tracker(page)
      @page_stats_trackers[page] = PageStatsTracker.new(page: page)
    end
  end
end
