RSpec.describe 'Listing webpages with most views' do
  subject { SpTest::WebsiteMonitorClient.new }

  describe 'given a simplest logfile has been loaded' do
    let(:simplest_logfile_path) { "#{Dir.pwd}/spec/logfiles_examples/simple.log" }

    before :each do
      subject.load(file_path: simplest_logfile_path)
    end

    describe 'when generating most visited pages report' do
      let(:report) { subject.generate_report(:most_views) }

      it 'includes one page' do
        expect(report.entries.size).to eq(1)
      end
    end

    describe 'when trying to generate unknown report' do
      it 'raises error' do
        expect do
          subject.generate_report(:highest_throughput)
        end.to raise_error(SpTest::Error)
      end
    end
  end

  describe 'given a logfile with multiple logs' do
    let(:logfile_path) { "#{Dir.pwd}/spec/logfiles_examples/multiple_logs.log" }

    before :each do
      subject.load(file_path: logfile_path)
    end

    describe 'when generating most visited pages report' do
      let(:report) { subject.generate_report(:most_views) }

      it 'includes all 5 pages' do
        expect(report.entries.size).to eq(5)
      end

      it 'most viewed page is "/help_page/1" with 4 visits' do
        expect(report.entries.first.page).to eq('/help_page/1')
        expect(report.entries.first.value).to eq(4)
        expect(report.entries.first.description).to eq('visits')
      end
    end

    describe 'when generating most uniquely viewed pages report' do
      let(:report) { subject.generate_report(:most_uniq_views) }

      it 'includes all 5 pages' do
        expect(report.entries.size).to eq(5)
      end

      it 'most uniquely viewed page is "/about/2" with 3 unique visits' do
        expect(report.entries.first.page).to eq('/about/2')
        expect(report.entries.first.value).to eq(3)
        expect(report.entries.first.description).to eq('unique views')
      end
    end
  end
end
