# frozen_string_literal: true

RSpec.describe SpTest::LogReader do
  subject { SpTest::LogReader.new }

  describe 'when reading empty file' do
    let(:empty_file) { "#{Dir.pwd}/spec/logfiles_examples/empty.log" }
    let(:page_views) { subject.read(empty_file) }

    it 'returns empty array' do
      expect(page_views).to be_empty
    end
  end

  describe 'when reading simple log file (single log entry)' do
    let(:simplest_logfile) { "#{Dir.pwd}/spec/logfiles_examples/simple.log" }
    let(:page_views) { subject.read(simplest_logfile) }

    it 'returns single page view' do
      expect(page_views.size).to eq(1)
      expect(page_views.first).to be_an_instance_of(SpTest::ValueObjects::PageView)
    end

    describe 'page view returned' do
      let(:page_view) { page_views.first }

      it 'has correctly parsed page name' do
        expect(page_view.page).to eq('/help_page')
      end

      it 'has correctly parsed client_id' do
        expect(page_view.client_id).to eq('126.318.035.038')
      end
    end
  end

  describe 'when reading log file with multiple logs' do
    let(:logfile_with_multiple_logs) { "#{Dir.pwd}/spec/logfiles_examples/multiple_logs.log" }
    let(:page_views) { subject.read(logfile_with_multiple_logs) }

    it 'parses all log entries' do
      expect(page_views.size).to eq(14)
    end

    describe 'first page view' do
      let(:page_view) { page_views.first }

      it 'has correctly parsed page name' do
        expect(page_view.page).to eq('/help_page/1')
      end

      it 'has correctly parsed client_id' do
        expect(page_view.client_id).to eq('451.106.204.921')
      end
    end
  end

  describe 'when reading log file with multiple empty lines at the end' do
    let(:logfile_with_multiple_empty_lines) { "#{Dir.pwd}/spec/logfiles_examples/multiple_empty_lines.log" }
    let(:page_views) { subject.read(logfile_with_multiple_empty_lines) }

    it 'parses all log entries correctly (and ignores empty lines)' do
      expect(page_views.size).to eq(5)
    end
  end

  describe 'when reading log file which does not exist' do
    let(:logfile_which_doesnt_exist) { "#{Dir.pwd}/spec/logfiles_examples/there_is_no_such.log" }

    it 'raises proper error' do
      expect do
        subject.read(logfile_which_doesnt_exist)
      end.to raise_error(SpTest::LogReader::FileNotFound)
    end
  end
end
