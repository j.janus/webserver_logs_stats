RSpec.describe SpTest::ReportGenerators do
  let(:page_stats) do
    [
      SpTest::ValueObjects::PageStats.new(page: '/', views: 103, uniq_views: 45),
      SpTest::ValueObjects::PageStats.new(page: '/home', views: 98, uniq_views: 60),
      SpTest::ValueObjects::PageStats.new(page: '/about', views: 10, uniq_views: 8),
      SpTest::ValueObjects::PageStats.new(page: '/careers', views: 213, uniq_views: 190),
      SpTest::ValueObjects::PageStats.new(page: '/team', views: 83, uniq_views: 64)
    ]
  end

  describe SpTest::ReportGenerators::MostViewsReportGenerator do
    subject { SpTest::ReportGenerators::MostViewsReportGenerator.new }

    describe 'when generating report for page stats' do
      let(:report) { subject.generate(page_stats) }

      it 'has "Most popular pages" name' do
        expect(report.name).to eql('Most popular pages')
      end

      it 'sorts stats correctly in descending order based on views' do
        expect(report.entries.map(&:page)).to eql(%w[/careers / /home /team /about])
      end

      it 'entries have correct description ("visits")' do
        expect(report.entries.map(&:description)).to all eql('visits')
      end

      it 'entries  have correct values' do
        expect(report.entries.map(&:value)).to eql([213, 103, 98, 83, 10])
      end
    end
  end

  describe SpTest::ReportGenerators::MostUniqueViewsReportGenerator do
    subject { SpTest::ReportGenerators::MostUniqueViewsReportGenerator.new }

    describe 'when generating report for page stats' do
      let(:report) { subject.generate(page_stats)}

      it 'has "Pages with most unique views" name' do
        expect(report.name).to eql('Pages with most unique views')
      end

      it 'sorts stats correctly in descending order based on unique views' do
        expect(report.entries.map(&:page)).to eql(%w[/careers /team /home / /about])
      end

      it 'entries have correct description ("unique views")' do
        expect(report.entries.map(&:description)).to all eql('unique views')
      end

      it 'entries  have correct values' do
        expect(report.entries.map(&:value)).to eql([190, 64, 60, 45, 8])
      end
    end
  end
end
