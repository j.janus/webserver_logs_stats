# frozen_string_literal: true

RSpec.describe SpTest::WebsiteMonitor do
  subject { SpTest::WebsiteMonitor.new }

  describe 'given a single page visit has been tracked' do
    let(:only_page_view) { SpTest::ValueObjects::PageView.new(page: '/home', client_id: '1.1.1.1') }

    before :each do
      subject.track_page_view(only_page_view)
    end

    it 'includes a page in all visited pages' do
      expect(subject.all_visited_pages_stats).to_not be_empty
      expect(subject.all_visited_pages_stats).to include(->(ps) { ps.page == '/home' })
    end

    it 'home page has one visit recorded' do
      home_page_stats = subject.stats_for('/home')
      expect(home_page_stats.views).to eq(1)
    end
  end

  describe 'given a couple of different page views' do
    let(:page_views) do
      [
        SpTest::ValueObjects::PageView.new(page: '/about', client_id: '1.1.1.1'),
        SpTest::ValueObjects::PageView.new(page: '/contact', client_id: '1.1.1.2'),
        SpTest::ValueObjects::PageView.new(page: '/', client_id: '1.1.1.3'),
        SpTest::ValueObjects::PageView.new(page: '/contact', client_id: '1.1.1.4')
      ]
    end

    before :each do
      page_views.each { |page_view| subject.track_page_view(page_view) }
    end

    it 'all 3 pages are present in stats' do
      expect(subject.all_visited_pages_stats.size).to eq(3)
    end

    it '"/contact" page has two views' do
      contact_page_stats = subject.stats_for('/contact')
      expect(contact_page_stats.views).to eq(2)
    end

    it 'not visited page (for example "/legal") ha no stats' do
      expect(subject.stats_for('/legal')).to be_nil
    end
  end

  describe 'given some pages have been visited by the same user (recognized by client_id)' do
    let(:client_a) { '1.1.1.1' }
    let(:client_b) { '1.1.1.2' }
    let(:page_views) do
      [
        SpTest::ValueObjects::PageView.new(page: '/', client_id: client_a),
        SpTest::ValueObjects::PageView.new(page: '/contact', client_id: client_a),
        SpTest::ValueObjects::PageView.new(page: '/contact', client_id: client_a),
        SpTest::ValueObjects::PageView.new(page: '/careers', client_id: client_a),
        SpTest::ValueObjects::PageView.new(page: '/about', client_id: client_b),
        SpTest::ValueObjects::PageView.new(page: '/', client_id: client_b)
      ]
    end

    before :each do
      page_views.each { |page_view| subject.track_page_view(page_view) }
    end

    it 'page visited twice by same client has 2 views in stats' do
      expect(subject.stats_for('/contact').views).to eq(2)
    end

    it 'page visited twice by same client has 1 unique view in stats' do
      expect(subject.stats_for('/contact').uniq_views).to eq(1)
    end

    it 'page visited twice by different clients has 2 unique views in stats' do
      expect(subject.stats_for('/').uniq_views).to eq(2)
    end
  end
end
